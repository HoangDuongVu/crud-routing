<?php
include_once('dbconnect.php');
$result  = $db->query('select * from users');


?>
<!DOCTYPE html>
<html>
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="container">
    <div class="msg">
        <?php echo (!empty($_GET['msg']))? $_GET['msg'] : "" ?>
    </div>
    <table class="table table-hover">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
        <?php
        $row=1;
        while($row){
            $row=$result->fetch_assoc();
            if (!$row['name']=="") {
        ?>

                <tr>
                    <td><?php echo $row['id'] ?></td>
                    <td><?php echo $row['name'] ?></td>
                    <td><?php echo $row['email'] ?></td>
                    <td><?php echo $row['phone'] ?></td>
                    <td><a href="<?php echo "http://127.0.0.1/Duong/1.Montreal-College/Islam-class/CRUD-routing-MySQL/index.php?action=update_form&id=" . $row['id']  ?>">Edit</a></td>
                    <td><a href="<?php echo "http://127.0.0.1/Duong/1.Montreal-College/Islam-class/CRUD-routing-MySQL/index.php?action=delete&id=" . $row['id']  ?>">Delete</a></td>

                </tr>
            
            <?php
            }
        }
        ?>
         <tr>
            <td><a href="<?php echo "http://127.0.0.1/Duong/1.Montreal-College/Islam-class/CRUD-routing-MySQL/index.php?action=create_form" ?>" >Create</a></td>
        </tr>
    </table>
</div>
</body>
</html>

