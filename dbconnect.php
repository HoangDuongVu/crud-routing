<?php
	
	class myDB{
		private $link;
		private $sqlQuery;
		public function __construct($hostname, $user, $pass, $db){
			
			$this->link = new  mysqli($hostname, $user, $pass, $db);

			if ($this->link->connect_errno) 
			{
			    echo("Connetion error");
        		exit();
			}
			
		}
		public function query($sqlQuery){
		    $this->lastQuery = $sqlQuery;
			return $this->link->query($sqlQuery);
		}

		public function getInstance() {
			return $this->link;

		}
    
	}
	
		
	$db= new myDB('localhost', 'root', '', 'ei_exercises');
	
	//$result = $db->query("SELECT * FROM users");
?>